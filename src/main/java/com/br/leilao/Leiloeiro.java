package com.br.leilao;

import java.util.List;

public class Leiloeiro {

    private String nome;
    private Leilao leilao;

    public Leiloeiro() {
    }

    public Leiloeiro(String nome, Leilao leilao) {
        this.nome = nome;
        this.leilao = leilao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public Lance retornarMaiorLance(){
        List<Lance> lances = this.leilao.getLances();
        double maiorValor = 0.0;
        Lance lanceVencedor = new Lance();
        for(Lance l : lances){
            if(l.getValorDoLance()>maiorValor){
                maiorValor = l.getValorDoLance();
                lanceVencedor.setUsuario(l.getUsuario());
                lanceVencedor.setValorDoLance(l.getValorDoLance());
            }
        }
        return lanceVencedor;
    }
}
