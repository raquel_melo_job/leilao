package com.br.leilao;

import java.util.ArrayList;
import java.util.List;

public class Leilao {

    private List<Lance> lances;

    public Leilao() {
        this.lances = new ArrayList<>();
    }

    public Leilao(Lance lance) {
        this.lances = new ArrayList<>();
        this.lances.add(lance);
    }

    public List<Lance> getLances() {
        return lances;
    }

    public Double getMaiorLance() {
        Double maior = 0.0;
        for(Lance l : this.lances){
            if(l.getValorDoLance()>maior){
                maior = l.getValorDoLance();
            }
        }
        return maior;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }

    public Lance adicionarNovoLance(Lance lance){
        this.lances.add(lance);
        return lance;
    }

    public boolean validarLance(Lance lance){
        double maiorValor = getMaiorLance();
        if(lance.getValorDoLance() > maiorValor ){
            this.lances.add(lance);
            return true;
        }
        return false;
    }
}
