package com.br.leilao;

public class Evento {

    public static void main(String[] args) {

        //INICIO DO EVENTO
        Usuario donoDoLeilao = new Usuario(1,"Chefe");
        Lance lanceInicial = new Lance(donoDoLeilao,1.50);
        Leilao leilao = new Leilao(lanceInicial);
        Leiloeiro leiloeiro = new Leiloeiro("Leiloeiro",leilao);

        //SIMULACAO DE 1 JOGO
        Usuario apostador = new Usuario(1,"Raquel");
        Lance lance = new Lance(apostador,10.50);

        if(leilao.validarLance(lance)){
            leilao.adicionarNovoLance(lance);
        }else{
            System.out.println("Valor abaixo do maior valor proposto");
        }

        //FINAL DO EVENTO
        System.out.println("O vencedor é : "+leiloeiro.retornarMaiorLance().getUsuario().getNome() + " com o lance de "+
                leiloeiro.retornarMaiorLance().getValorDoLance()+" reais!");

    }
}
