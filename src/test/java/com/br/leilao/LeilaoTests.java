package com.br.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.temporal.ValueRange;

public class LeilaoTests {

    @Test
    public void testarAdicaoDeNovoLance(){
        Leilao leilao = new Leilao();
        Usuario apostador = new Usuario(1,"Raquel");
        Lance lance = new Lance(apostador,10.50);

        Assertions.assertEquals(leilao.adicionarNovoLance(lance),lance);
    }

    @Test
    public void testarNovoLeilaoMaiorQueOAnterior(){
        //INICIO DO EVENTO
        Usuario donoDoLeilao = new Usuario(1,"Chefe");
        Lance lanceInicial = new Lance(donoDoLeilao,10.50);
        Leilao leilao = new Leilao(lanceInicial);
        Leiloeiro leiloeiro = new Leiloeiro("Leiloeiro",leilao);

        //SIMULACAO DE 1 JOGO
        Usuario apostador = new Usuario(1,"Raquel");
        Lance lance = new Lance(apostador,15.50);

        Assertions.assertEquals(leilao.validarLance(lance),true);
    }

    @Test
    public void testarNovoLeilaoMenorQueOAnterior(){
        //INICIO DO EVENTO
        Usuario donoDoLeilao = new Usuario(1,"Chefe");
        Lance lanceInicial = new Lance(donoDoLeilao,10.50);
        Leilao leilao = new Leilao(lanceInicial);
        Leiloeiro leiloeiro = new Leiloeiro("Leiloeiro",leilao);

        //SIMULACAO DE 1 JOGO
        Usuario apostador = new Usuario(1,"Raquel");
        Lance lance = new Lance(apostador,5.50);

        Assertions.assertFalse(leilao.validarLance(lance));
        //Assertions.assertThrows(NumberFormatException.class,() -> {leilao.validarLance(lance);},"Valor proposto abaixo do maior valor");
    }

    @Test
    public void testarORetornoDoMaiorLance(){
        Usuario donoDoLeilao = new Usuario(1,"Chefe");
        Lance lanceInicial = new Lance(donoDoLeilao,1.50);
        Leilao leilao = new Leilao(lanceInicial);
        Leiloeiro leiloeiro = new Leiloeiro("Leiloeiro",leilao);

        Usuario apostador = new Usuario(1,"Raquel");
        Lance lance = new Lance(apostador,10.50);
        leilao.adicionarNovoLance(lance);

        Assertions.assertEquals(leiloeiro.retornarMaiorLance().getValorDoLance(),lance.getValorDoLance());
    }
}
